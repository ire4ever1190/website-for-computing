## **Piracy**
: - Online copyright piracy is the practise of illegally producing and sharing infromation on the internet such as music or software


## **Online Streaming Service**
: - An online video streaming service is an on demand online entertainment source for TV shows, movies and other digital media


## **Streaming**
: - Refers to the method of viewing media where the content is slowly downloaded


## **Copyright**
: - Is a form of a intellectual property that grants the creator of an original creative work and exclusive legal right to determine whether and under what conditions that original work my be used by 
others


## **VCS**
: - Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later.

## **BitTorrent**
: - A communication protocol for peer-to-peer file sharing (P2P) which is used to distribute data and electronic files over the Internet. It is one of the most common protocols for transferring large 
files, such as digital video files containing TV shows or video clips or digital audio files containing songs.

## **Peer to Peer File Sharing (P2P)**
: - The distribution and sharing of digital media using peer-to-peer (P2P) networking technology. P2P file sharing allows users to access media files such as books, music, movies, and games using a P2P 
software program that searches for other connected computers on a P2P network to locate the desired content. The nodes (peers) of such networks are end-user computers and distribution servers.

 
## **ISP**
: - An Internet service provider (ISP) is an organization that provides services for accessing, using, or participating in the Internet. Internet service providers may be organized in various forms, 
such as commercial, community-owned, non-profit, or otherwise privately owned.
