---
---

### Planning
___
To start off the planning of the project we used "Gantt Project" to make 
a Gantt chart to plan out when we were going to do tasks.

{{<slideimg src="/img/ganttchart.png" alt="project plan">}}

We then thought for a bit of what our issue was until Jake came up with 
the idea to use a modified version of his year 11 SAT hypothesis

We then brain stormed some cause and effects of the our idea

{{<img src="/img/brainstorm.png" alt="brainstorm">}}

### Data Gathering
___
Once we had the plan we started to gather some data. Joel and Owen gathered the secondary data while Jake made a survey for the primary data.
Joel and Owen found a large fata set about internet traffic online from [sandvine internet phenomena report](https://www.sandvine.com/resources) which was then validated and put into an excel 
document. Jake made the survey in [Microsoft forms](https://forms.office.com/). He used that over others like Google forms and Survey Monkey since he liked the branching feature
which allowed him to only show relevant questions.


### Website Development
___
We decided to use a static site generator in the beginning since we knew it would be easy to use and decrease development time.
we decided on [hugo](https://gohugo.io/) due to its speed and wider range of themes that suited our needs.
Once we decided on that we thought up of a design idea on how we wanted out site to look.

{{<img src="/img/sitedesign.png" alt="brainstorm">}}

Once we had done that we looked around to see if there was any themes that were close to these.
We looked at these three themes.

- [hugo primer](https://github.com/qqhann/hugo-primer)
- [hugo book](https://github.com/alex-shpak/hugo-book)
- [Piercer](https://github.com/danielkvist/hugo-piercer-theme)

we settled on hugo book since it fit all our criteria the best.

From there, Jake setup the server to host the website. He used [nginx](https://www.nginx.com/) to host the static content. The static files were built locally on his laptop
and uploaded to the server with the use of [syncthing](https://syncthing.net/) which let us near instantly see changes on the website.
To help protect his server, reduce loading times and reduce server usage, Jake used [cloudflare](https://cloudflare.com).

git was used as a VCS to keep track of development and backup the code.
The rest of the data and code was also backed up to Jake's syncthing network which consisted of two servers in separate locations, his phone and his laptop which meant that
the data and code could never be lost. 

Once the site was up and running, Jake spent some time optimising the 
theme since there was a few milliseconds that he knew he could shave off 
the loading time. After a bit of fiddling with the CSS he finally got 
the [lighthouse](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk?hl=en) performance score to 98 and 1 second faster load times.

He also reduced the server load by adding caching for the images which 
decreased load times and server load.

