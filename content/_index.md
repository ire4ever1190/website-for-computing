---
title: Introduction
type: docs
---

# **Internet Piracy**
___
By Jake, Joel, and Owen

Internet piracy is a major issue for content creators that has been around since the start of the internet. It involves the illegal distribution of copyrighted content usually through the use of 
BitTorrent which allows people to share files without the use of a server to host the download. 

## Stakeholders
___
The main people involved in piracy are content producers like actors, directors, artists, programmers, and other personnel that help create the content and relies on the company to make money, and 
pirates.
Content producers are more affected by internet piracy since it reduces their profits and sales figures and can even cause [companies to close 
down](https://www.macgasm.net/2012/12/04/ios-game-battle-dungeon-forced-to-shut-down-due-to-piracy/). Pirates are unaffected negatively in this since they are the ones who are stealing the content.

## Legal & Social Aspects
___
Piracy is illegal in near all but a few countries. In Australia for example, if your ISP catches you torrenting copyrighted content then they are required to report that to the copyright holder who can 
take you to court over it.  
The social aspects can vary from person to person. Some peoples social groups might not care about the person pirating or might even enjoy it since they get free media. Others might frown upon the 
persons actions since they don't like how they are not giving money to the content creators.



The best way to see why people still pirate is to look at the decline and rise of internet piracy

## **The decline of piracy**
___

Back when Netflix first launched its streaming service back in 2010 there was a decrease in piracy in the United States.

{{<img src="/img/ustrendsgoogle.png" alt="United States search trends">}}

The positive correlation between Netflix and Torrent related searches before 2011 though does show that their DVD service wasn't available enough for piracy to stop.

The trend of Torrent related searches dropping when Netflix launched their streaming service is also mirrored in Australia which saw a sharp decrease in piracy when Netflix streaming service was 
launched in 2015.

{{<img src="/img/autrendsgoogle.png" alt="Australian search trends">}}

This data shows that Netflix did lead to a decrease in piracy most likely due to better availability of content.

[Data from Sandvine internet phenomena reports](https://www.sandvine.com/resources) also showed a negative correlation between Netflix and BitTorrent traffic in Northern America.
{{< img src="/img/sandvine.png" alt="Sandvine Internet Traffic">}}


## **The rise of piracy**
___

But that data is either old or incorrect. Google stopped indexing websites related to piracy in 2018 and people pirate with VPNs which means that that traffic would not show up in their own countries search trends or on Sandvine's data.

Has other companies start their competing streaming services, more content gets locked behind exclusive paywalls which cause a massive separation in the availability of content

The recent release of Game of Thrones Season 8 showed how these exclusives increased piracy. A [report from Torrent Freak](https://torrentfreak.com/premiere-of-final-game-of-thrones-season-triggers-piracy-bonanza-190415/)
Highlighted that near instantly after Season 8 came out, people were already pirating it in large numbers. This is most likely due to Game of Thrones being limited to certain streaming services
which cost extra money to the user which they don't want to spend on only one TV show.

To get a better idea of why people were pirating the group set up a survey to understand if people still pirate after getting a streaming service. The survey asked questions about what streaming they used and if they pirated before and after getting that streaming service. This survey was then sent out to [r/VCE](https://www.reddit.com/r/vce) and [r/Australia](https://www.reddit.com/r/australia) as well as to all year 11 students at St Patricks College. 206 people responded to this survey

Near all respondants owned some streaming service of sorts.

{{<img src="/img/services.png" alt="Respondants streaming service">}}

and near 75% of them pirated before getting a streaming service.
of the 36 people who don't have a streaming service, near 75% of them also pirate content.

of the 119 people who pirated before getting a streaming service, near 75% of them still pirate content.
All of those people left responses on why they still pirate content.
Some of the responses included

* "Because more streaming services are being created with too many exclusives. Rather than buying all the exclusives I want, I can pirate it"
* "Availability and price"
* "Not available on Netflix or Stan, or when a show comes out with a new ep every week and there’s no other way to watch it because it comes out in America."

These feelings are echoed in other responses and show that increased price and decreased availability drive people to Piracy
